// ==UserScript==
// @name         Blacklist for Gamekyo
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Cachez les publications des membres que vous ne souhaitez plus voir.
// @author       Bourbon
// @match        https://www.gamekyo.com/
// @match        https://www.gamekyo.com/index.html
// @icon         https://www.google.com/s2/favicons?sz=64&domain=gamekyo.com
// @grant        none
// ==/UserScript==

const links = document.querySelectorAll('a[href=""], a[href=""]');

for (const link of links) {
  const parentDiv = link.parentNode.parentNode.parentNode;
  parentDiv.style.display = 'none';
}
