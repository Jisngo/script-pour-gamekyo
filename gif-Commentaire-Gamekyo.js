// ==UserScript==
// @name         Gif-Commentaire-Gamekyo
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  Pour afficher les images et les gifs dans les commentaires
// @author       Bourbon
// @match        https://www.gamekyo.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=gamekyo.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

// Sélection de la div avec la classe "comment-list"
const commentList = document.querySelector('.comment-list');

// Sélection de tous les liens présents dans les commentaires
const liensCommentList = commentList.querySelectorAll('a');

liensCommentList.forEach(lien => {
  const url = lien.getAttribute('href');

  // Vérifie si l'URL se termine par .gif, .png, .jpeg, ou .jpg
  if (url) {
    const extension = url.toLowerCase().split('.').pop(); // Obtenez l'extension de l'URL

    if (['gif', 'png', 'jpeg', 'jpg'].includes(extension)) {
      // Créez un nouvel élément image
      const imageElement = document.createElement('img');

      // Défini l'URL de l'image comme source de l'image
      imageElement.src = url;

      // Dimensions de l'image
      imageElement.style.width = '400px';
      imageElement.style.height = 'auto';

      // Remplace le lien par l'image
      lien.parentNode.replaceChild(imageElement, lien);
    }
  }
});
})();
