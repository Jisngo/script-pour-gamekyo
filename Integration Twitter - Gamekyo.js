// ==UserScript==
// @name         Twitter intégration - Gamekyo
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  Intégrer Twitter dans ses articles Gamekyo
// @author       Bourbon
// @match        https://www.gamekyo.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=gamekyo.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // Fonction pour traiter un lien
    function processLink(lien) {
        if (lien.href.includes('twitter.com') || lien.href.includes('x.com')) {
            // Modifie le lien si c'est de type "x.com"
            if (lien.href.includes('x.com')) {
                lien.href = lien.href.replace('x.com', 'twitter.com');
            }

            // Ajoute la partie https://twitframe.com/show?url= devant le lien
            const lienModifie = 'https://twitframe.com/show?url=' + lien.href;

            // Crée un élément iframe pour intégrer le lien Twitter
            const iframe = document.createElement('iframe');
            iframe.src = lienModifie;
            iframe.width = '500'; // Vous pouvez définir la largeur souhaitée
            iframe.height = '400px'; // Vous pouvez définir la hauteur souhaitée
            iframe.style.border = 'none';

            // Remplace le lien par l'iframe
            lien.parentNode.replaceChild(iframe, lien);
        }
    }

    // Vérifie si l'URL de la page correspond à l'un des modèles spécifiés
    const currentPageURL = window.location.href;
    if (currentPageURL.match(/^https:\/\/www\.gamekyo\.com\/(blog_article|groupnews_article|group_article)\d+\.html$/)) {
        // Sélectionne la div avec la classe "medium text" ou "article-text-text"
        const mediumTextDiv = document.querySelector('#article-blogging-text-text, #article-text-text');

        // Vérifie si la div existe sur la page
        if (mediumTextDiv) {
            // Sélectionne tous les liens dans la div
            const liensDansMediumText = mediumTextDiv.querySelectorAll('a');

            // Parcourez tous les liens
            liensDansMediumText.forEach(processLink);
        }

        // Sélectionne toutes les div.comment-box
        const commentBoxes = document.querySelectorAll('div.comment-box');

        // Parcoure toutes les div.comment-box
        commentBoxes.forEach(commentBox => {
            // Sélectionne tous les liens dans la div.comment-box
            const liensDansCommentBox = commentBox.querySelectorAll('a');

            // Parcoure tous les liens dans la div.comment-box
            liensDansCommentBox.forEach(processLink);
        });
    }
})();
