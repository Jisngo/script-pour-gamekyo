// ==UserScript==
// @name         Mode sombre - Gamekyo
// @namespace    http://tampermonkey.net/
// @version      0.8
// @description  Passez Gamekyo dans le côté obscur de la force !
// @author       Bourbon
// @match        https://www.gamekyo.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=gamekyo.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // Définir une fonction pour appliquer un style à un élément
    function appliquerStyle(element, styles) {
        for (const property in styles) {
            element.style[property] = styles[property];
        }
    }

    // Styles communs
    const styleDefaut = {
        backgroundColor: "#121212",
        color: "#FFFFFF"
    };
    // Styles pour les liens
    const styleLiens = {
        color: "#FFF"
    };

    const styleSecondaire = {
        backgroundColor: "#0b0b0c",
    };
    // Styles pour les boutons de vote
    const styleBoutonVote = {
        backgroundColor: "#F08A28",
        borderColor: "#F08A28"
    };

    // Appliquer les styles aux éléments

    // Couleurs barre de navigation
    appliquerStyle(document.querySelector('#main-menu-lign'), styleDefaut);
    appliquerStyle(document.querySelector('#sub-menu-lign'), styleDefaut);
    const notification = document.querySelector('#notification-allrequest');
    if (notification) {
        appliquerStyle(notification, styleDefaut);
        }

    // Couleur de fond Gamekyo
    appliquerStyle(document.body, styleSecondaire);

    // Couleur liens
    const couleurLiens = document.querySelectorAll('a');
    couleurLiens.forEach(lien => appliquerStyle(lien, styleLiens));

    // Boutons de vote
    const voterLinks = document.querySelectorAll('.voters-link');
    voterLinks.forEach(voter => appliquerStyle(voter, styleBoutonVote));

    // Options commentaires
    const optionsCommentaires = document.querySelectorAll('.tagSelection, #hover');
    optionsCommentaires.forEach(option => appliquerStyle(option, styleDefaut));

    // Zones de textes
    const zoneTextes = document.querySelectorAll('.textarea');
    zoneTextes.forEach(zoneTexte => {
        appliquerStyle(zoneTexte, {
            backgroundColor: "#121212",
            backgroundImage: "none",
            color: "#FFF"
        });
    });

var spans = document.getElementsByTagName("span");

// Recherche des balises span possédant un titre = à click to edit
for (var i = 0; i < spans.length; i++) {
    if (spans[i].getAttribute("title") === "click to edit") {
        // Balises span trouvé
        var spanAvecTitleClickToEdit = spans[i];

        // Ajoute un évenement mouseover pour édition de texte et surligne le texte en gris
        spanAvecTitleClickToEdit.addEventListener("mouseover", function() {
            this.style.backgroundColor = "#999999";
        });

        // Retire l'événement mouseover pour l'édition de texte
        spanAvecTitleClickToEdit.addEventListener("mouseout", function() {
            this.style.backgroundColor = "";
        });
    }
}

    // Couleurs des publications (blog, news...)
    const couleurBackground = document.querySelectorAll('#article-blogging-title-text, #article-title-text, #category_id, #Jeu_choix, .tip-container, .small-thumb-collection li, #profile-post-information, .big-thumb, .medium-thumb, #gallery-media-container, .yellow-background, .gray-background, .white-background, .box-frame, .post-box, .box-container, .post-information, .post-list, .option-link, #message-list, .orange-background, .message-post-triangle, .administration-menu-category, .green-background, .box-container-external');

    couleurBackground.forEach(background => appliquerStyle(background, styleDefaut));

    // Couleurs détails et textes
    const details = document.querySelectorAll('.date, .medium-text, .article-details');
    details.forEach(detail => appliquerStyle(detail, styleDefaut));

    // Entête colonne
    const enteteColonne = document.querySelectorAll('.box-title-external-blue, .box-title');
    enteteColonne.forEach(entete => {
        appliquerStyle(entete, {
            backgroundColor: "#121212",
            border: "solid 1px #FFF"
        });
    });

    // Bas colonne
    const basColonne = document.querySelectorAll('.box-bottom-link');
    basColonne.forEach(bas => appliquerStyle(bas, styleDefaut));

    // Titres blog
    const titresBlog = document.querySelectorAll('.small-title a, .big-title a');
    titresBlog.forEach(titres => {
        titres.style.setProperty('color', '#FFF', 'important');
    });

    // Commentaires
    const commentaires = document.querySelectorAll('.comment-box');
    commentaires.forEach(commentaire => appliquerStyle(commentaire, styleDefaut));

    const commentairesLiens = document.querySelectorAll('.comment-box a');
    commentairesLiens.forEach(commentaireLien => {
        commentaireLien.style.color = "#54A6D5";
    });

    // Article container
    appliquerStyle(document.querySelector('.article-container'), styleDefaut);

    // Box frames
    const boxFrames = document.querySelectorAll('.box-frame, .box-container');
    boxFrames.forEach(boxFrame => {
        appliquerStyle(boxFrame, {
            backgroundColor: "#121212",
            color: "#FFF"
        });
    });

    // Onglets
    const tabs = document.querySelectorAll('.yellow-background, .gray-background');
    tabs.forEach(tab => appliquerStyle(tab, styleDefaut));

    // Boîte d'administration
    const adminBox = document.querySelectorAll('.administration-menu-category');
    adminBox.forEach(admin => {
        appliquerStyle(admin, {
            backgroundColor: "#121212",
            border: "none"
        });
    });

    // Citations
    const citations = document.querySelectorAll('#quote');
    citations.forEach(citation => {
        citation.style.cssText += 'background-color: #0b0b0c !important;';
    });
})();
