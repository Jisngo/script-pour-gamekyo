// ==UserScript==
// @name         blacklist mots clés
// @namespace    http://tampermonkey.net/
// @version      0.8
// @description  try to take over the world!
// @author       Bourbon
// @match        https://www.gamekyo.com/
// @match        https://www.gamekyo.com/index.html
// @icon         https://www.google.com/s2/favicons?sz=64&domain=gamekyo.com
// @grant        none
// ==/UserScript==

const blog = document.getElementById('blog-list');
const mots = ['motcle1', 'motcle2'];
const links = blog.querySelectorAll('a');

for (const link of links) {
  for (const mot of mots) {
    if (link.title.toLowerCase().includes(mot)) {
      const parentDiv = link.parentNode.parentNode;
      parentDiv.style.display = 'none';
      break;
    }
  }
}